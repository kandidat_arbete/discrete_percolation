#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

int main(int argc, char* args[]) {
  //The first argument is the length of the percolation square,
  //the other is probability for edge existing
  if (argc != 3) {
    std::cout << "Needs exactly two arguments!\n";
    return 1;
  }

  //Convert first argument to integer length
  int length = atoi(args[1]);
  if (length <= 0) {
    std::cout << "First arg is length: integer and positive.\n";
    return 1;
  }

  //Convert second argument to double p
  char** nullptr = 0;
  double p = strtod(args[2], nullptr);
  if (p < 0 || p > 1) {
    std::cout << "Second arg is probability: real and in [0, 1]\n";
    return 1;
  }

  //Seed generator
  srand(time(NULL));

  //Populate the lattice
  bool vert[length*length], hori[length*length];

  for (int i = 0; i < length*length; i++) {
      vert[i] = (((float) rand()) / RAND_MAX < p); 
      hori[i] = (((float) rand()) / RAND_MAX < p);
  }

  //Output to tikz
  std::cout << "\\begin{figure}\n" << "\\begin{center}\n";
  std::cout << "\\begin{tikzpicture}[scale=" << 10.0/length << "]\n";

  for (int y = 0; y < length; y++) {
    for (int x = 0; x < length; x++) {
      printf("\\filldraw[fill=black] (%d,%d) circle [radius=0.1];\n", x, y);

      if (hori[length*y + x] && x+1 != length)
        printf("\\draw (%d,%d) -- (%d,%d);\n", x, y, x+1, y);

      if (vert[length*y + x] && y+1 != length) 
        printf("\\draw (%d,%d) -- (%d,%d);\n", x, y, x, y+1);
      
    }
  } 

  std::cout << "\\end{tikzpicture}\n";
  std::cout << "\\caption{Discrete percolation with length "
    << length << " and probability " << p << ".}\n";
  std::cout << "\\end{center}\n" << "\\end{figure}\n";

  return 0;
}

